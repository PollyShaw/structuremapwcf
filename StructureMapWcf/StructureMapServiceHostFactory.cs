﻿using System;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace StructureMapWcf
{
    public class StructureMapServiceHostFactory : ServiceHostFactory
    {
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            return new StructureMapServiceHost(serviceType, baseAddresses);
        }
    }
}