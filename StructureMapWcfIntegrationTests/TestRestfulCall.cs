﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace StructureMapWcfIntegrationTests
{
    [TestFixture]
    public class TestRestfulCall
    {
        private Process _process;
        private int _freePort;

        [SetUp]
        public void SetUp()
        {
            _freePort = GetFreePort();

            _process = new Process();
            _process.StartInfo = new ProcessStartInfo(Path.GetFullPath(@"..\..\..\Build\IIS Express\IISExpress.exe"), 
                        String.Format("/port:{0} /path:\"{1}\"", _freePort, Path.GetFullPath(@"..\..\..\StructureMapWcfTestWebsite")));
            _process.StartInfo.RedirectStandardInput = true;
            _process.StartInfo.UseShellExecute = false;
            _process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            _process.Start();

        }

        [TearDown]
        public void TearDown()
        {
            _process.Kill();
        }

        private int GetFreePort()
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            var ipEndPoint = new IPEndPoint(IPAddress.Any, 0);
            socket.Bind(ipEndPoint);
            try
            {
                return ((IPEndPoint) socket.LocalEndPoint).Port;
            }
            finally
            {
                socket.Close();
            }
        }

        [Test]
        public void Test()
        {
            WebRequest webRequest =
                WebRequest.CreateHttp(String.Format("http://localhost:{0}/TestService.svc/String", _freePort));
            var response = webRequest.GetResponse();
            var streamReader = new StreamReader(
                response.GetResponseStream());
            var responseText = streamReader.ReadToEnd();
            StringAssert.Contains("TestService Hello", responseText);
        }


    }
}
